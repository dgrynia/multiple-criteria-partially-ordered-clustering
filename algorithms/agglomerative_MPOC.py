import numpy as np
from utils.common_tools import get_alternative_to_cluster_dict, partition_numbers_to_labels


def get_relation_from_outranking_matrix_numpy(i, j, outranking_matrix):
    i_S_j = outranking_matrix[i, j]
    j_S_i = outranking_matrix[j, i]

    if i_S_j and not j_S_i:
        return "Pij"
    elif not i_S_j and j_S_i:
        return "Pji"
    elif i_S_j and j_S_i:
        return "I"
    return "R"


def fast_PQ_measure(partition, outranking_matrix_alternatives_numpy, outranking_matrix_clusters_numpy, alpha_p=1, alpha_i=1, alpha_r=1, verbose=False):
    n = len(outranking_matrix_alternatives_numpy)
    vp = vi = vr = 0

    alternative_to_cluster = get_alternative_to_cluster_dict(partition)

    for i in range(n):
        for j in range(i + 1, n):
            ci, cj = alternative_to_cluster[i], alternative_to_cluster[j]

            alternatives_relation = get_relation_from_outranking_matrix_numpy(i, j, outranking_matrix_alternatives_numpy)
            clusters_relation = get_relation_from_outranking_matrix_numpy(ci, cj, outranking_matrix_clusters_numpy)

            if alternatives_relation == "Pij" and clusters_relation == "Pji" or alternatives_relation == "Pji" and clusters_relation == "Pij":
                vp += 1
            elif alternatives_relation == "R" and clusters_relation == "I":
                vi += 1
            elif alternatives_relation == "I" and clusters_relation == "R":
                vr += 1

    quality = 1 - 2 * (alpha_p * vp + alpha_i * vi + alpha_r * vr) / (n * (n - 1))
    return quality


def fast_RCI_measure(partition, outranking_matrix_alternatives_numpy, outranking_matrix_clusters_numpy, verbose=False):
    n = len(outranking_matrix_alternatives_numpy)
    alternative_to_cluster = get_alternative_to_cluster_dict(partition)

    num_pairs = 0
    opposite_preference = 0
    I_R_inconsistencies = 0
    P_R_inconsistencies = 0
    P_I_inconsistencies = 0

    for i in range(n):
        for j in range(i + 1, n):
            num_pairs += 1
            ci, cj = alternative_to_cluster[i], alternative_to_cluster[j]

            alternatives_relation = get_relation_from_outranking_matrix_numpy(i, j, outranking_matrix_alternatives_numpy)
            clusters_relation = get_relation_from_outranking_matrix_numpy(ci, cj, outranking_matrix_clusters_numpy)

            if alternatives_relation in ["Pij", "Pji"] and clusters_relation == "I" or alternatives_relation == "I" and clusters_relation in ["Pij", "Pji"]:
                P_I_inconsistencies += 1  # distance: 2
            elif alternatives_relation == "I" and clusters_relation == "R" or alternatives_relation == "R" and clusters_relation == "I":
                I_R_inconsistencies += 1  # distance: 2
            elif alternatives_relation in ["Pij", "Pji"] and clusters_relation == "R" or alternatives_relation == "R" and clusters_relation in ["Pij", "Pji"]:
                P_R_inconsistencies += 1  # distance: 3
            elif alternatives_relation == "Pij" and clusters_relation == "Pji" or alternatives_relation == "Pji" and clusters_relation == "Pij":
                opposite_preference += 1  # distance: 4

    consistency_index = 1 - 0.25 * (4 * opposite_preference +
                                    3 * P_R_inconsistencies +
                                    2 * P_I_inconsistencies +
                                    2 * I_R_inconsistencies) / num_pairs

    return consistency_index


def fast_outranking_degree(cluster1, cluster2, outranking_matrix):
    degree = 0
    for i in cluster1:
        for j in cluster2:
            degree += outranking_matrix[i, j]
    degree /= len(cluster1) * len(cluster2)
    return degree


def calculate_clusters_relations(partition, outranking_matrix_alternatives_numpy, cluster_outranking_threshold=0.5):
    n = len(partition)
    outranking_matrix = np.identity(n, dtype=int)
    for i in range(n):
        for j in range(n):
            if i != j:
                degree = fast_outranking_degree(partition[i], partition[j], outranking_matrix_alternatives_numpy)
                outranking_matrix[i, j] = degree >= cluster_outranking_threshold

    return outranking_matrix


def agglomarative_MPOC_PQ_clustering(alternatives, X, n_clusters, outranking_matrix_alternatives, verbose=False, **kwargs):
    alternatives_numbers = list(range(len(alternatives)))
    outranking_matrix_alternatives_numpy = outranking_matrix_alternatives.to_numpy()
    partition = np.array(alternatives_numbers).reshape(len(alternatives_numbers), 1).tolist()
    if verbose:
        print(partition, "\n")

    while len(partition) > n_clusters:
        if verbose:
            print("\n----------------")
        best_first_candidate = None
        best_second_candidate = None
        best_quality = -1
        for i in range(len(partition)):
            if best_quality == 1:
                break
            for j in range(i + 1, len(partition)):
                first_candidate = partition[i]
                second_candidate = partition[j]
                simulated_merge = partition.copy()
                simulated_merge.pop(j)
                simulated_merge.pop(i)
                merged = first_candidate + second_candidate
                simulated_merge.append(merged)
                if verbose:
                    print(simulated_merge)
                simulated_merge_clusters_relations_numpy = calculate_clusters_relations(simulated_merge, outranking_matrix_alternatives_numpy)
                quality = fast_PQ_measure(simulated_merge, outranking_matrix_alternatives_numpy, simulated_merge_clusters_relations_numpy)
                if verbose:
                    print(quality)
                if quality > best_quality:
                    best_quality = quality
                    best_first_candidate = first_candidate
                    best_second_candidate = second_candidate
                    if best_quality == 1:
                        break
        if verbose:
            print("applying merge", [alternatives[i] for i in best_first_candidate], [alternatives[i] for i in best_second_candidate])
        new_partition = partition.copy()
        new_partition[new_partition.index(best_first_candidate)] = sorted(best_first_candidate + best_second_candidate)
        new_partition.remove(best_second_candidate)
        partition = new_partition

    return partition_numbers_to_labels(partition, alternatives)


def agglomarative_MPOC_RCI_clustering(alternatives, X, n_clusters, outranking_matrix_alternatives, verbose=False, **kwargs):
    alternatives_numbers = list(range(len(alternatives)))
    outranking_matrix_alternatives_numpy = outranking_matrix_alternatives.to_numpy()
    partition = np.array(alternatives_numbers).reshape(len(alternatives_numbers), 1).tolist()
    if verbose:
        print(partition, "\n")

    while len(partition) > n_clusters:
        if verbose:
            print("\n----------------")
        best_first_candidate = None
        best_second_candidate = None
        best_quality = -1
        for i in range(len(partition)):
            if best_quality == 1:
                break
            for j in range(i + 1, len(partition)):
                first_candidate = partition[i]
                second_candidate = partition[j]
                simulated_merge = partition.copy()
                simulated_merge.pop(j)
                simulated_merge.pop(i)
                merged = first_candidate + second_candidate
                simulated_merge.append(merged)
                if verbose:
                    print(simulated_merge)
                simulated_merge_clusters_relations_numpy = calculate_clusters_relations(simulated_merge, outranking_matrix_alternatives_numpy)
                quality = fast_RCI_measure(simulated_merge, outranking_matrix_alternatives_numpy, simulated_merge_clusters_relations_numpy)
                if verbose:
                    print(quality)
                if quality > best_quality:
                    best_quality = quality
                    best_first_candidate = first_candidate
                    best_second_candidate = second_candidate
                    if best_quality == 1:
                        break
        if verbose:
            print("applying merge", [alternatives[i] for i in best_first_candidate], [alternatives[i] for i in best_second_candidate])
        new_partition = partition.copy()
        new_partition[new_partition.index(best_first_candidate)] = sorted(best_first_candidate + best_second_candidate)
        new_partition.remove(best_second_candidate)
        partition = new_partition

    return partition_numbers_to_labels(partition, alternatives)
