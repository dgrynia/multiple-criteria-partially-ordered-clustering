from utils.common_tools import get_input_matrix


def classical_algorithm_adaptation_outranking_matrix(alternatives, X, num_clusters, outranking_matrix_alternatives, seed, input_matrix_type, classical_clustering_algorithm, verbose=False, **kwargs):
    input_matrix = get_input_matrix(outranking_matrix_alternatives, input_matrix_type)
    partition = classical_clustering_algorithm(alternatives, input_matrix, num_clusters, outranking_matrix_alternatives, seed=seed)
    if isinstance(partition, tuple):
        partition = partition[0]

    return partition
