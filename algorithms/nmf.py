import json
from sklearn.decomposition import NMF

from utils.common_tools import get_input_matrix, partition_from_predicted_labels, filter_out_empty_clusters

with open("datasets/nmf_k_dict.json") as json_file:
    nmf_k_dict = json.load(json_file)


def nmf_clustering_sklearn(alternatives, X, num_clusters, outranking_matrix_alternatives, input_matrix_type, dataset_name, seed, verbose=False, **kwargs):
    key = str((input_matrix_type, dataset_name, seed, num_clusters))
    if key not in nmf_k_dict:
        return nmf_clustering_sklearn(alternatives, X, num_clusters - 1, outranking_matrix_alternatives, input_matrix_type, dataset_name, seed, verbose=False, **kwargs)
    k = nmf_k_dict[key]

    input_matrix = get_input_matrix(outranking_matrix_alternatives, input_matrix_type)

    nmf = NMF(n_components=k, init='nndsvd', solver="cd", random_state=seed, max_iter=10000)
    W = nmf.fit_transform(input_matrix)
    pred = W.argmax(axis=1)
    partition = partition_from_predicted_labels(alternatives, pred)

    return filter_out_empty_clusters(partition, verbose=False)
