import numpy as np
import random
from collections import Counter
from pprint import pprint

import json
from more_itertools import sort_together
from collections import Counter
from utils.common_tools import *


def calculate_alternatives_profiles(alternatives, outranking_matrix_alternatives):
    outranking_matrix_alternatives_numpy = outranking_matrix_alternatives.to_numpy()

    profiles = {}
    for a1, alternative1 in enumerate(alternatives):
        R_ai = []
        P_minus_ai = []
        I_ai = []
        P_plus_ai = []
        for a2, alternative2 in enumerate(alternatives):
            a1_S_a2 = outranking_matrix_alternatives_numpy[a1, a2]
            a2_S_a1 = outranking_matrix_alternatives_numpy[a2, a1]

            if a1_S_a2 and not a2_S_a1:
                P_plus_ai.append(alternative2)
            elif not a1_S_a2 and a2_S_a1:
                P_minus_ai.append(alternative2)
            elif a1_S_a2 and a2_S_a1:
                I_ai.append(alternative2)
            elif not a1_S_a2 and not a2_S_a1:
                R_ai.append(alternative2)
            else:
                assert False, "dla pewności, ale to sie nie może wydarzyć"

        profiles[alternative1] = {
            "R_ai": set(R_ai),
            "P_minus_ai": set(P_minus_ai),
            "I_ai": set(I_ai),
            "P_plus_ai": set(P_plus_ai)
        }
    return profiles


def ekme_distance(ai_profile, aj_profile, n_alternatives):
    numerator = 0
    for key in ['R_ai', 'P_minus_ai', 'I_ai', 'P_plus_ai']:
        numerator += len(ai_profile[key].intersection(aj_profile[key]))

    distance = 1 - (numerator / n_alternatives)
    return distance


def kmeans_extension_single_run(alternatives, dataset, n_clusters, outranking_matrix_alternatives, seed, initial_centroids=None, max_iter=200, verbose=False):
    if initial_centroids is None:
        initial_centroids = dataset.sample(n_clusters, replace=False, random_state=seed).index.tolist()
    centroids = initial_centroids

    # Creating a profile for each alternative
    profiles = calculate_alternatives_profiles(alternatives, outranking_matrix_alternatives)

    # The profile consists of 4 elements: 'R_ai', 'P_minus_ai', 'I_ai', 'P_plus_ai'
    keys = ['R_ai', 'P_minus_ai', 'I_ai', 'P_plus_ai']

    # The profiles of centroids, initially selected from existing alternatives profiles
    centroids_profiles = []
    for centroid in centroids:
        centroids_profiles.append(profiles[centroid])

    partition = [[] for _ in centroids]
    partition_history = []

    rng = np.random.default_rng(seed=seed)

    iternum = 0
    while iternum < max_iter:
        iternum += 1
        partition = [[] for _ in centroids]

        for alternative in alternatives:
            min_distance = np.inf
            closest_centroid_idx = None
            centroids_indices_random_order = list(range(n_clusters))
            rng.shuffle(centroids_indices_random_order)
            for centroid_idx in centroids_indices_random_order:
                distance = ekme_distance(profiles[alternative], centroids_profiles[centroid_idx], len(alternatives))
                if distance < min_distance:
                    min_distance = distance
                    closest_centroid_idx = centroid_idx

            partition[closest_centroid_idx].append(alternative)

        # ------------- The stopping condition -- no change in 3 subsequent iterations -------------
        # The last 3 partitions are identical -- NOTE, due to randomness, the algorithm may cycle
        if iternum > 3 and partition == partition_history[-1] == partition_history[-2]:
            break

        # The current result has already appeared at least 3 times in the history (this way it is possible to break the cycle)
        if iternum > 3:
            current_partition_repetitions = 0
            for p in partition_history:
                if p == partition:
                    current_partition_repetitions += 1

            if current_partition_repetitions >= 3:
                break

        partition_history.append(partition.copy())

        # ------------- solving the empty cluster problem -------------
        if not all(partition):
            if verbose:
                print("one of the clusters is empty:", partition, iternum)

        while not all(partition):
            max_distance = -np.inf
            max_distance_alternative = ""
            max_distance_cluster_idx = None

            for centroid_idx, cluster in enumerate(partition):
                if len(cluster) < 2:
                    continue
                for alternative in cluster:
                    distance = ekme_distance(profiles[alternative], centroids_profiles[centroid_idx], len(alternatives))
                    if distance > max_distance:
                        max_distance = distance
                        max_distance_alternative = alternative
                        max_distance_cluster_idx = centroid_idx
            if verbose:
                print(f"moving alternative {max_distance_alternative} from cluster {max_distance_cluster_idx} to cluster {partition.index([])}")
            try:
                partition[max_distance_cluster_idx].remove(max_distance_alternative)
                partition[partition.index([])].append(max_distance_alternative)
            except:
                print(partition, "\n", max_distance, max_distance_alternative, max_distance_cluster_idx,
                      centroids_profiles,
                      profiles,
                      outranking_matrix_alternatives)
                assert False

        # ------------- updating the profile of each centroid -------------
        # by voting in which of the 4 sets each alternative should be placed
        if verbose:
            print("\nbefore voting")
            pprint(centroids_profiles)
        for centroid_idx in range(len(centroids_profiles)):
            updated_profile = {'R_ai': [], 'P_plus_ai': [], 'P_minus_ai': [], 'I_ai': []}

            votes = {}
            for alternative in alternatives:
                votes[alternative] = {'R_ai': 0, 'P_plus_ai': 0, 'P_minus_ai': 0, 'I_ai': 0}
            for alternative in partition[centroid_idx]:
                for key in keys:
                    for alternative2 in profiles[alternative][key]:
                        votes[alternative2][key] += 1

            for alternative in alternatives:
                max_value = max(votes[alternative].values())
                candidates = [k for k in votes[alternative] if votes[alternative][k] == max_value]
                k = candidates[0] if len(candidates) < 2 else rng.choice(candidates)  # according to paper, in case of a tie "final value will be randomly chosen"
                updated_profile[k].append(alternative)

            for key in keys:
                updated_profile[key] = set(updated_profile[key])
            centroids_profiles[centroid_idx] = updated_profile

    if verbose:
        print("partition history:")
        pprint(partition_history)

    return partition


def kmeans_extension_repeated_run(alternatives, X, n_clusters, outranking_matrix_alternatives, seed, n_repetitions=100, verbose=False):
    seeds = generate_seeds(seed, n_repetitions)
    results = []

    from tqdm.notebook import tqdm
    for seed in tqdm(seeds, leave=False):
        partition = kmeans_extension_single_run(alternatives, X, n_clusters, outranking_matrix_alternatives, seed=seed, verbose=verbose)
        results.append(json.dumps(sorted(partition, key=lambda x: x[0])))

    most_common_partition = json.loads(Counter(results).most_common()[0][0])
    return most_common_partition
