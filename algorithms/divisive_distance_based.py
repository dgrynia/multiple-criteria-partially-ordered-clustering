import numpy as np
import pandas as pd
from utils.common_tools import natsort, get_alternative_to_cluster_dict, partition_numbers_to_labels
from utils.distance_measures import calculate_distance_matrix_in_criteria_space


def calculate_average_distance_to_cluster(alternative, cluster, distance_measure):
    if alternative in cluster:
        if len(cluster) == 1:
            return 0.0
        denominator = (len(cluster) - 1)
    else:
        denominator = len(cluster)

    distance_to_cluster = 0
    for a2 in cluster:
        distance_to_cluster += distance_measure(alternative, a2)

    return distance_to_cluster / denominator


def reassign_to_closest_cluster(alternatives_numbers, partition, distance_measure, verbose=False):
    if verbose:
        print("starting reassignment procedure")

    num_clusters = len(partition)
    reassignment_history = []

    while True:
        alternative_to_cluster = get_alternative_to_cluster_dict(partition)
        reassignments = []
        
        for alternative in alternatives_numbers:
            min_avg_distance = np.inf
            new_cluster_idx = -1
            for cluster_idx in range(num_clusters):
                avg_distance = calculate_average_distance_to_cluster(alternative, partition[cluster_idx], distance_measure)
                if avg_distance < min_avg_distance:
                    min_avg_distance = avg_distance
                    new_cluster_idx = cluster_idx

            current_cluster_idx = alternative_to_cluster[alternative]
            if current_cluster_idx != new_cluster_idx:
                if verbose:
                    print("reassign", alternative, "from", current_cluster_idx, "to", new_cluster_idx)
                partition[current_cluster_idx].remove(alternative)
                partition[new_cluster_idx].append(alternative)
                reassignments.append((alternative, current_cluster_idx, new_cluster_idx))

        if not reassignments:  # no more reassignments needed, it's time to stop
            if verbose:
                print("reassignement DONE -- no more changes")
            break

        # cycle detection
        cycle_detected = False
        for previous_reassignments in reversed(reassignment_history):
            if reassignments == previous_reassignments:
                cycle_detected = True
                break

        if cycle_detected:
            if verbose:
                print("reassignement procedure stopped -- cycle detected")
                print(previous_reassignments, reassignments)
            break  # cycle detected, breaking while loop
        reassignment_history.append(reassignments)

        if verbose:
            print("reassignment -- sorting clusters")
        for cluster in partition:
            cluster.sort()

    return partition


def divisive_clustering(alternatives, X, num_clusters, outranking_matrix_alternatives, affinity="euclidean", distance_matrix=None, verbose=False, **kwargs):
    if distance_matrix is None:
        distance_matrix = calculate_distance_matrix_in_criteria_space(X, metric=affinity)
        distance_matrix = distance_matrix.to_numpy()
        distance_measure = lambda ai, aj: distance_matrix[ai, aj]
    elif isinstance(distance_matrix, pd.DataFrame):
        distance_matrix = distance_matrix.to_numpy()
        distance_measure = lambda ai, aj: distance_matrix[ai, aj]
    else:
        raise TypeError("distance_matrix must be pd.DataFrame or None, not {}".format(type(distance_matrix)))

    alternatives_numbers = list(range(len(alternatives)))
    partition = [alternatives_numbers.copy()]

    while len(partition) < num_clusters:
        max_distance = 0
        max_distance_pair = None
        cluster_to_divide = 0
        for cluster_idx, cluster in enumerate(partition):
            if len(cluster) > 1:  # cluster containing only one object cannot be divided
                for i in range(len(cluster)):
                    for j in range(i + 1, len(cluster)):
                        distance = distance_measure(cluster[i], cluster[j])
                        if distance > max_distance:
                            max_distance = distance
                            max_distance_pair = (cluster[i], cluster[j])
                            cluster_to_divide = cluster_idx

        if max_distance < 0.0000000001:  # stop when greatest distance is equal to 0
            break

        first_part = [max_distance_pair[0]]
        second_part = [max_distance_pair[1]]
        old_cluster = partition.pop(cluster_to_divide)
        old_cluster.remove(max_distance_pair[0])
        old_cluster.remove(max_distance_pair[1])
        for alternative in old_cluster:
            if distance_measure(alternative, first_part[0]) < distance_measure(alternative, second_part[0]):
                first_part.append(alternative)
            else:
                second_part.append(alternative)

        partition.append(sorted(first_part))
        partition.append(sorted(second_part))

        if verbose:
            print("partition before reassignment:", partition)
        partition = reassign_to_closest_cluster(alternatives_numbers, partition, distance_measure, verbose)
        if verbose:
            print("partition after reassignment;", partition)

    return partition_numbers_to_labels(partition, alternatives)
