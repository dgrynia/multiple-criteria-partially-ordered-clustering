import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from utils.common_tools import partition_numbers_to_labels


def find_cluster_to_divide(partition, outranking_matrix_alternatives_numpy, verbose=False):
    max_num_of_inconsistencies = 0
    cluster_to_divide = 0
    inconsistency_matrix = None
    for cluster_idx, cluster in enumerate(partition):
        if len(cluster) > 1:
            num_of_inconsistencies = 0
            P_inc = R_inc = 0
            inconsistency_matrix_temp = np.zeros((len(cluster), len(cluster)), dtype=int)
            for i in range(len(cluster)):
                for j in range(i + 1, len(cluster)):
                    ai, aj = cluster[i], cluster[j]
                    ai_S_aj = outranking_matrix_alternatives_numpy[ai, aj]
                    aj_S_ai = outranking_matrix_alternatives_numpy[aj, ai]

                    if ai_S_aj and not aj_S_ai:  # ai P aj
                        num_of_inconsistencies += 1
                        inconsistency_matrix_temp[i, j] = 1
                        inconsistency_matrix_temp[j, i] = 1
                        P_inc += 1

                    elif aj_S_ai and not ai_S_aj:  # aj P ai
                        num_of_inconsistencies += 1
                        inconsistency_matrix_temp[i, j] = 1
                        inconsistency_matrix_temp[j, i] = 1
                        P_inc += 1

                    elif not ai_S_aj and not aj_S_ai:  # ai R aj
                        num_of_inconsistencies += 1
                        inconsistency_matrix_temp[i, j] = 1
                        inconsistency_matrix_temp[j, i] = 1
                        R_inc += 1

                    elif ai_S_aj and aj_S_ai:
                        pass  # This is not an inconsistency, we want to have indifferent alternatives within one cluster.

            if verbose:
                print("cluster", cluster_idx, "num inconsistencies", num_of_inconsistencies)
                print("P inconsistencies", P_inc, "\nR inconsistencies", R_inc)
            if num_of_inconsistencies > max_num_of_inconsistencies:
                max_num_of_inconsistencies = num_of_inconsistencies
                inconsistency_matrix = inconsistency_matrix_temp
                cluster_to_divide = cluster_idx

    return max_num_of_inconsistencies, inconsistency_matrix, cluster_to_divide


def divide_cluster(old_cluster, inconsistency_matrix, verbose=False):
    index_translator = res = dict(zip(old_cluster, list(range(len(old_cluster)))))

    first_part = []
    second_part = []
    for a1 in old_cluster:

        first_part_inconsistency = 0
        for a2 in first_part:
            first_part_inconsistency += inconsistency_matrix[index_translator[a1], index_translator[a2]]

        second_part_inconsistency = 0
        for a2 in second_part:
            second_part_inconsistency += inconsistency_matrix[index_translator[a1], index_translator[a2]]

        if verbose:
            print(a1, "first/second part inconsistency", first_part_inconsistency, second_part_inconsistency, end="")

        if first_part_inconsistency < second_part_inconsistency:
            first_part.append(a1)
            if verbose:
                print("  <")
        else:
            second_part.append(a1)
            if verbose:
                print("  >")

    return first_part, second_part


def divisive_intra_cluster_relations_clustering(alternatives, X, num_clusters, outranking_matrix_alternatives, verbose=False, show_plot=False, **kwargs):
    alternatives_numbers = list(range(len(alternatives)))
    outranking_matrix_alternatives_numpy = outranking_matrix_alternatives.to_numpy()
    partition = [alternatives_numbers.copy()]

    while len(partition) < num_clusters:
        max_num_of_inconsistencies, inconsistency_matrix, cluster_to_divide = find_cluster_to_divide(partition, outranking_matrix_alternatives_numpy, verbose)

        if max_num_of_inconsistencies == 0:
            break

        if verbose:
            print("splitting cluster", cluster_to_divide, "num inconsistencies", max_num_of_inconsistencies)
            print(pd.DataFrame(inconsistency_matrix, index=partition[cluster_to_divide], columns=partition[cluster_to_divide]), inconsistency_matrix.sum())

        old_cluster = partition.pop(cluster_to_divide)
        first_part, second_part = divide_cluster(old_cluster, inconsistency_matrix, verbose)
        partition.append(sorted(first_part))
        partition.append(sorted(second_part))

        if show_plot:
            fig, ax = plt.subplots(figsize=(3, 6))
            for cluster in partition:
                print(cluster)
                points = X[X.index.isin(cluster)].to_numpy()
                ax.scatter(*zip(*points))

            ax.grid()
            plt.show()

    return partition_numbers_to_labels(partition, alternatives)
