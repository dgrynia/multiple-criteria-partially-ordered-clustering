import numpy as np
from kmodes.kmodes import KModes
from utils.common_tools import partition_from_predicted_labels, filter_out_empty_clusters


def kmodes_clustering(alternatives, X, n_clusters, outranking_matrix_alternatives, seed):
    km = KModes(n_clusters=n_clusters, init='Cao', n_init=1, random_state=seed, verbose=False)
    pred = km.fit_predict(X.to_numpy().astype(np.int32))

    partition = partition_from_predicted_labels(alternatives, pred)
    centroids = km.cluster_centroids_

    partition, centroids = filter_out_empty_clusters(partition, centroids)

    return partition, centroids
