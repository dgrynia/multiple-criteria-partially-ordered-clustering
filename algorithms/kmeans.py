import json
from more_itertools import sort_together
from collections import Counter
from utils.common_tools import *
from sklearn.cluster import KMeans


def kmeans_single_run(alternatives, X, n_clusters, outranking_matrix_alternatives, seed):
    kmea = KMeans(n_clusters=n_clusters, init="random", n_init=1, algorithm="lloyd", random_state=seed).fit(X)

    pred = kmea.labels_
    partition = partition_from_predicted_labels(alternatives, pred)
    centroids = kmea.cluster_centers_

    partition, centroids = filter_out_empty_clusters(partition, centroids)

    return partition, centroids


def kmeans_repeated_run(alternatives, X, n_clusters, outranking_matrix_alternatives, seed, n_repetitions=100, verbose=False, **kwargs):
    seeds = generate_seeds(seed, n_repetitions)
    results = []

    for seed in seeds:
        partition, centroids = kmeans_single_run(alternatives, X, n_clusters, outranking_matrix_alternatives, seed=seed)
        results.append(json.dumps(sort_together([partition, centroids.tolist()], key_list=(0,), key=lambda x: x[0])))

    most_common_partition, most_common_centroids = json.loads(Counter(results).most_common()[0][0])
    return most_common_partition, np.array(most_common_centroids, dtype=float)
