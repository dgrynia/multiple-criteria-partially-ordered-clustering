from utils.common_tools import *
from sklearn.cluster import AgglomerativeClustering


def agglomerative_clustering(alternatives, X, num_clusters, outranking_matrix_alternatives, affinity="euclidean", distance_matrix=None, verbose=False, **kwargs):
    if affinity != "precomputed":
        aggl = AgglomerativeClustering(n_clusters=num_clusters, metric=affinity, linkage="average").fit(X)
    else:
        aggl = AgglomerativeClustering(n_clusters=num_clusters, metric=affinity, linkage="average").fit(distance_matrix)

    pred = aggl.labels_
    partition = partition_from_predicted_labels(alternatives, pred)

    return partition
