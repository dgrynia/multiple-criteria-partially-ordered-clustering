from pyclustering.cluster.somsc import somsc
from utils.common_tools import filter_out_empty_clusters
import numpy as np


def som_clustering(alternatives, X, num_clusters, outranking_matrix_alternatives, seed, verbose=False, **kwargs):
    somsc_instance = somsc(X.to_numpy().astype(float).copy(), num_clusters)
    somsc_instance.process()
    predictions = somsc_instance.get_clusters()

    alternatives = np.array(alternatives)
    partition = []
    for cluster in predictions:
        partition.append(alternatives[cluster].tolist())

    return filter_out_empty_clusters(partition)
