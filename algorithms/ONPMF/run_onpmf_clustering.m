real_datasets_files = fullfile("outranking_matrices/real_datasets", {dir("outranking_matrices/real_datasets/*.csv").name})
artificial_datasets_files = fullfile("outranking_matrices/artificial_datasets", {dir("outranking_matrices/artificial_datasets/*.csv").name})
files = horzcat(real_datasets_files, artificial_datasets_files)

mkdir results_onpmf_outgoing_relations
mkdir results_onpmf_incoming_relations
mkdir results_onpmf_all_relations

for input_matrix_type_cell = {'outgoing_relations', 'incoming_relations', 'all_relations'}
    input_matrix_type = input_matrix_type_cell{1};
    fprintf("%s\n", input_matrix_type)
    results_dict = struct();
     
    for csv_path = files
        for num_clusters = 2:8
            [filepath, filename, ext] = fileparts(csv_path);
            json_filename = strcat("results_onpmf_", input_matrix_type, "/", filename, "_", int2str(num_clusters), ".json");
            json_filename = strrep(json_filename, ".csv", ".json");
            if isfile(json_filename) == 1
                fprintf("%s exists, skipping ...\n", json_filename)
                continue;
            end
            
            fprintf("%s \t %d \n", filename, num_clusters)
            outranking_matrix_alternatives = csvread(csv_path, 1, 1);
            
            max_iter = 3000;
            
            tic();
            tStart = cputime();
            predicted_labels = onpmf_clustering(num_clusters, outranking_matrix_alternatives, input_matrix_type, max_iter);
            tEnd = cputime();
            tictoc = toc();
            elapsed = tEnd - tStart;
            
            results_dict = struct("predicted_labels", predicted_labels, "processing_time", elapsed, "tictoc", tictoc);
            fid = fopen(json_filename, 'w');
            fprintf(fid, jsonencode(results_dict)); 
            fclose(fid);
        end
    end
end




function [input_matrix] = get_input_matrix(outranking_matrix_alternatives, input_matrix_type)
    if strcmp(input_matrix_type, "outgoing_relations") == 1
        % outgoing relations (alternatives outrankeg by a_i) are included in the row a_i, so we simply use outranking matrix
        input_matrix = outranking_matrix_alternatives;
    elseif strcmp(input_matrix_type, "incoming_relations") == 1
        % incoming relations (alternatives that outrank a_i) are contained in column a_i, so we use transposed matrix
        input_matrix = outranking_matrix_alternatives.';
    elseif strcmp(input_matrix_type, "all_relations") == 1
        % Concatenate a matrix with a transposed matrix, the resulting matrix has 2n binary attributes,
        % each row contains information about the alternatives a_i outranks and the alternatives thet ourtank a_i
        input_matrix = horzcat(outranking_matrix_alternatives, outranking_matrix_alternatives.');
    else
        error("Only 'incoming_relations', 'outgoing_relations' and 'all_relations' are supported")
    end
end

function [predicted_labels] = onpmf_clustering(num_clusters, outranking_matrix_alternatives, input_matrix_type, max_iter)
    if nargin < 4
      max_iter = 3000;
    end

    input_matrix = get_input_matrix(outranking_matrix_alternatives, input_matrix_type);
    num_clusters = min(size(unique(input_matrix,'rows'), 1), num_clusters);
    [Uonpmf, V, relError, actualIters] = onpmf(input_matrix, num_clusters, max_iter);
    
    [~, predicted_labels] = max(Uonpmf, [], 2);
end