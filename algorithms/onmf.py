import json
import numpy as np
from pathlib import Path

from utils.common_tools import partition_from_predicted_labels, filter_out_empty_clusters

seed_to_seed_idx_dict = {
    211382627: 0,
    1366902868: 1,
    1751793295: 2,
    826376757: 3,
    802808611: 4,
    101888266: 5,
    137839923: 6,
    2051389637: 7,
    473707324: 8,
    1945729569: 9,
    784281680: 10,
    981334646: 11,
    1196259392: 12,
    602441052: 13,
    864575919: 14,
    1366119152: 15,
    190978421: 16,
    2106060168: 17,
    1041697791: 18,
    1744683975: 19,
    613512360: 20,
    655408239: 21,
    1294256005: 22,
    1219794242: 23,
    830642888: 24,
    44425363: 25,
    1179191336: 26,
    1230723935: 27,
    542761800: 28,
    885174175: 29
}

def onpmf_clustering(alternatives, X, num_clusters, outranking_matrix_alternatives, input_matrix_type, dataset_name, seed, verbose=False, **kwargs):
    seed_idx = seed_to_seed_idx_dict[seed]
    fname = f"algorithms/ONPMF/results_onpmf_{input_matrix_type}/{Path(dataset_name).stem}_{seed_idx}_{num_clusters}.json"
    
    try:
        with open(fname) as json_file:
            onpmf_result = json.load(json_file)
            pred = np.array(onpmf_result["predicted_labels"]) - 1  # MATLAB's array indexing start from 1
            processing_time = onpmf_result["tictoc"]
            partition = partition_from_predicted_labels(alternatives, pred)
    except FileNotFoundError as e:
        print(e)
        print("To perform ONMF clustering, please first run the MATLAB script located at: algorithms/ONPMF/run_onpmf_clustering.m")

    return filter_out_empty_clusters(partition, verbose=True), processing_time
