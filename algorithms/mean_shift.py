import numpy as np
from sklearn.metrics import pairwise_distances_argmin_min
from utils.common_tools import partition_from_predicted_labels
import warnings
import json


def gaussian_kernel(distances, bandwidth):
    return (1 / ((2 * np.pi) ** 0.5 * bandwidth)) * np.exp(-0.5 * (distances / bandwidth) ** 2)


def flat_kernel(distances, radius):
    return (distances <= radius).astype(float)


def truncated_gaussian_kernel(distances, radius, bandwidth):
    return gaussian_kernel(distances, bandwidth) * flat_kernel(distances, radius)


def shift(x, X, radius, bandwidth):
    distances = np.sqrt(((x - X) ** 2).sum(axis=1))  # Euclidean distances between x and all other points in X
    weights = truncated_gaussian_kernel(distances, radius, bandwidth)
    new_x = (weights @ X) / weights.sum()
    return new_x


def unique_rows(A):
    remove = np.zeros(A.shape[0], dtype=bool)
    for i in range(A.shape[0]):
        equals = np.all(np.isclose(A[i, :], A[(i + 1):, :], rtol=1e-05, atol=1e-8), axis=1)
        remove[(i + 1):] = np.logical_or(remove[(i + 1):], equals)
    return A[np.logical_not(remove)]


def meanshift_clustering(alternatives, X, num_clusters, outranking_matrix_alternatives, look_distance=None, kernel_bandwidth=None, max_iter=1000, dataset_name=None, verbose=False, **kwargs):
    if dataset_name is not None:
        with open('datasets/mean_shift_parameters.json') as json_file:
            meanshift_params = json.load(json_file)
        radius = meanshift_params[dataset_name][str(num_clusters)]['look_distance']
        bandwidth = meanshift_params[dataset_name][str(num_clusters)]['kernel_bandwidth']

    X_original = X.to_numpy().astype(float).copy()

    X_past = X_original
    for it in range(1, max_iter + 1):
        X_new = np.array([shift(x, X_past, radius, bandwidth) for x in X_past])

        if np.allclose(X_past, X_new, rtol=1e-05, atol=1e-8):
            if verbose:
                print(f"centroids no longer move or the differences are negligible, {it} iterations performed")
            break
        X_past = X_new

    if it == max_iter:
        warnings.warn(f"The iteration limit has been reached, although the algorithm has not reached convergence, look_distance={look_distance},  kernel_bandwidth={kernel_bandwidth}")

    centroids = unique_rows(X_new)
    predictions, _ = pairwise_distances_argmin_min(X_original, centroids)
    partition = partition_from_predicted_labels(alternatives, predictions)

    return partition, centroids
