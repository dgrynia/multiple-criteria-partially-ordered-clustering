from utils.common_tools import *
from sklearn_extra.cluster import KMedoids


def kmedoids_clustering(alternatives, X, n_clusters, outranking_matrix_alternatives, affinity="euclidean", distance_matrix=None, seed=42, verbose=False):
    if affinity == "precomputed":
        kmedoids = KMedoids(n_clusters=n_clusters, method="pam", init="build", max_iter=1000, metric=affinity, random_state=seed).fit(distance_matrix)
    else:
        kmedoids = KMedoids(n_clusters=n_clusters, method="pam", init="build", max_iter=1000, metric=affinity, random_state=seed).fit(X)

    pred = kmedoids.labels_
    partition = partition_from_predicted_labels(alternatives, pred)
    centroids = X.iloc[kmedoids.medoid_indices_].to_numpy()

    partition, centroids = filter_out_empty_clusters(partition, centroids)

    return partition, centroids
