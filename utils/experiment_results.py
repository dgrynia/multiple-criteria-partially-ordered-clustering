class ExperimentResultsCollector:
    def __init__(self, algorithm_description, X, master_seed, n_electre_configurations, verbose):
        self.algorithm_description = algorithm_description
        self.X = X
        self.master_seed = master_seed
        self.n_electre_configurations = n_electre_configurations
        self.verbose = verbose

        self.seeds_list = []
        self.electre_parameters_list = []

        self.partition_list = []
        self.centroids_list = []
        self.outranking_matrix_alternatives_list = []
        self.partitioning_execution_time_list = []

    def collect_signle_run_parameters(self, seed, indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda):
        self.seeds_list.append(seed)
        self.electre_parameters_list.append((indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda))

    def collect_single_run_results(self, partition, centroids, outranking_matrix_alternatives, partitioning_execution_time):
        self.partition_list.append(partition)
        self.centroids_list.append(centroids)
        self.outranking_matrix_alternatives_list.append(outranking_matrix_alternatives)
        self.partitioning_execution_time_list.append(partitioning_execution_time)

        if self.verbose:
            print("\n---------------------------------------------------------------\n---------------------------------------------------------------\n")
