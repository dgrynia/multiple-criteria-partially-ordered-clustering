import numpy as np
import pandas as pd
import re

natsort = lambda s: [int(t) if t.isdigit() else t.lower() for t in re.split('(\d+)', s)]
import warnings


def calculate_centroids_for_partition(X, partition):
    centroids = []

    for cluster in partition:
        centroids.append(X.loc[cluster].to_numpy().mean(axis=0))

    return np.array(centroids)


def get_alternative_to_cluster_dict(partition):
    alternative_to_cluster = {}
    for i, cluster in enumerate(partition):
        for alternative in cluster:
            alternative_to_cluster[alternative] = i

    return alternative_to_cluster


def partition_from_predicted_labels(alternatives, pred):
    partition = []
    for i in range(max(pred) + 1):
        partition.append(np.array(alternatives)[pred == i].tolist())

    return partition


def partition_to_labels(partition):
    alternative_to_label_dict = {}
    for cluster_idx, cluster in enumerate(partition):
        for alternative in cluster:
            alternative_to_label_dict[alternative] = cluster_idx

    labels = [alternative_to_label_dict[key] for key in sorted(alternative_to_label_dict.keys(), key=natsort)]
    return labels


def partition_numbers_to_labels(partition_numbers, alternatives_labels):
    partition = []
    for cluster_numbers in partition_numbers:
        partition.append([])
        for alternative_number in cluster_numbers:
            partition[-1].append(alternatives_labels[alternative_number])

    return partition


def filter_out_empty_clusters(partition, centroids=None, verbose=False):
    if not all(partition):
        if verbose:
            warnings.warn("partition zawiera puste klastry, filtrowanie...")
        if centroids is None:
            partition = [x for x in partition if x]
            return partition
        else:
            filtered_partition = []
            filtered_centroids = []
            for cluster, centroid in zip(partition, centroids):
                if cluster:
                    filtered_partition.append(cluster)
                    filtered_centroids.append(centroid)
            return filtered_partition, np.array(filtered_centroids)

    if centroids is None:
        return partition
    else:
        return partition, centroids


def generate_seeds(seed, n):
    rng = np.random.default_rng(seed=seed)
    seeds = rng.integers(low=0, high=np.iinfo(np.int32).max, size=n, dtype=np.int32)
    return seeds


def next_seed(seed):
    rng = np.random.default_rng(seed=seed)
    return rng.integers(low=0, high=np.iinfo(np.int32).max, dtype=np.int32)


def display_preference_matrix_colorful(preference_matrix_numpy):
    preference_matrix = preference_matrix_numpy.copy()
    preference_matrix[preference_matrix == "Pij"] = "P+"
    preference_matrix[preference_matrix == "Pji"] = "P-"
    preference_matrix[preference_matrix == None] = ""
    preference_matrix = pd.DataFrame(preference_matrix)

    color_dict = {
        "P+": "lightgreen",
        "P-": "pink",
        "I": "white",
        "R": "yellow",
        "": "white"
    }
    display(preference_matrix.style.applymap(lambda x: f'background-color : {color_dict[x]}'))


def draw_cluster_order(outranking_matrix_clusters):
    import networkx as nx
    from networkx.drawing.nx_pylab import draw_networkx
    import matplotlib.pyplot as plt
    plt.figure(figsize=(3, 3))
    graph = nx.DiGraph()

    for i in range(len(outranking_matrix_clusters)):
        for j in range(len(outranking_matrix_clusters)):
            if i == j:
                # graph.add_edge(str(i), str(j), color="red")
                continue
            if outranking_matrix_clusters.at[i, j]:
                graph.add_edge(str(i), str(j), color="black")

    colors = ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b", "#e377c2", "#7f7f7f", "#bcbd22", "#17becf"][:len(outranking_matrix_clusters)]
    # spring_layout seed is fixed so that for the same input data, the graph always looks the same.
    draw_networkx(graph, pos=nx.spring_layout(graph, seed=42), arrowsize=25, node_color=colors, with_labels=True, node_size=800)
    plt.show()


def get_relation_from_outranking_matrix(i, j, outranking_matrix):
    i_S_j = outranking_matrix.at[i, j]
    j_S_i = outranking_matrix.at[j, i]

    if i_S_j and not j_S_i:
        return "Pij"
    elif not i_S_j and j_S_i:
        return "Pji"
    elif i_S_j and j_S_i:
        return "I"
    return "R"


def get_relation_from_outranking_matrix_numpy(i, j, outranking_matrix):
    i_S_j = outranking_matrix[i, j]
    j_S_i = outranking_matrix[j, i]

    if i_S_j and not j_S_i:
        return "Pij"
    elif not i_S_j and j_S_i:
        return "Pji"
    elif i_S_j and j_S_i:
        return "I"
    return "R"


def get_input_matrix(outranking_matrix_alternatives, input_matrix_type):
    if input_matrix_type == "outgoing_relations":
        # Outgoing relations (alternatives outranked by a_i) are contained in the row a_i, so we simply operate on the outranking matrix
        input_matrix = outranking_matrix_alternatives
    elif input_matrix_type == "incoming_relations":
        # Incoming relations (alternatives that outrank a_i) are contained in column a_i, so we transpose the matrix
        input_matrix = outranking_matrix_alternatives.T
    elif input_matrix_type == "all_relations":
        # We concatenate an outranking matrix with a transposed outranking matrix, the resulting matrix has 2n columns (binary attributes),
        # so that each row of a_i contains complete information about the alternatives a_i outranks and the alternatives that outrank a_i.
        input_matrix = pd.merge(outranking_matrix_alternatives, outranking_matrix_alternatives.T, left_index=True, right_index=True)
    else:
        raise NotImplementedError("Only 'incoming_relations', 'outgoing_relations' and 'all_relations' are supported")

    return input_matrix


def outranking_matrix_to_preference_matrix(outranking_matrix_alternatives):
    outranking_matrix_alternatives_numpy = outranking_matrix_alternatives.to_numpy()
    alternatives = outranking_matrix_alternatives.index.tolist()
    n = len(alternatives)

    preference_matrix = np.empty((n, n), dtype=object)
    for i in range(n):
        for j in range(n):
            preference_matrix[i, j] = get_relation_from_outranking_matrix_numpy(i, j, outranking_matrix_alternatives_numpy)

    return preference_matrix
