import numpy as np
import pandas as pd
from algorithms.kmeans_extension import calculate_alternatives_profiles, ekme_distance


# Multiple criteria distance measure from Kmeans extension algorithm (De Smet & Guzman)
def calculate_distance_matrix_MC_distance(outranking_matrix_alternatives):
    distance_matrix = np.zeros_like(outranking_matrix_alternatives, dtype=float)

    alternatives_names = outranking_matrix_alternatives.index.tolist()

    # Creating a profile for each alternative
    profiles = calculate_alternatives_profiles(alternatives_names, outranking_matrix_alternatives)

    for i in range(len(alternatives_names)):
        for j in range(i + 1, len(alternatives_names)):
            ai, aj = alternatives_names[i], alternatives_names[j]
            d = ekme_distance(profiles[ai], profiles[aj], len(alternatives_names))
            distance_matrix[i, j] = d
            distance_matrix[j, i] = d

    return pd.DataFrame(distance_matrix, index=alternatives_names, columns=alternatives_names)


def calculate_distance_matrix_in_criteria_space(X, metric="euclidean"):
    from sklearn.metrics import pairwise_distances
    distance_matrix = pairwise_distances(X, metric=metric)

    return pd.DataFrame(distance_matrix, index=X.index.tolist(), columns=X.index.tolist())
