import numpy as np
import pandas as pd

from utils.common_tools import get_relation_from_outranking_matrix
from utils.electre_tools import calculate_credibility_and_outranking_matrix


def cluster_outranking_remove_indifferences(outranking_matrix, credibility_matrix, verbose):
    new_matrix = outranking_matrix.copy()

    for ci in range(len(new_matrix)):
        for cj in range(ci + 1, len(new_matrix)):
            clusters_relation = get_relation_from_outranking_matrix(ci, cj, new_matrix)
            if clusters_relation == "I":
                if credibility_matrix.at[ci, cj] < credibility_matrix.at[cj, ci]:
                    new_matrix.at[ci, cj] = 0
                    if verbose:
                        print("removing", ci, cj)
                else:
                    new_matrix.at[cj, ci] = 0
                    if verbose:
                        print("removing", cj, ci)

    return new_matrix


def cluster_outranking_remove_cycles(outranking_matrix, credibility_matrix, verbose):
    outranking_matrix = outranking_matrix.copy()
    np.fill_diagonal(outranking_matrix.values, 0)

    import networkx as nx
    while cycles := list(nx.simple_cycles(nx.from_numpy_array(outranking_matrix.to_numpy(), create_using=nx.DiGraph))):
        if verbose:
            print("cykle:", cycles)
        cycle = cycles[0]
        candidate_arcs_to_remove = []
        for i, j in zip(cycle, cycle[1:] + [cycle[0]]):
            candidate_arcs_to_remove.append((i, j, credibility_matrix.at[i, j]))

        candidate_arcs_to_remove.sort(key=lambda x: x[2])
        if verbose:
            print("candidates for removal:", candidate_arcs_to_remove)
            print(credibility_matrix, "\n", outranking_matrix)
        arc_to_remove = candidate_arcs_to_remove[0]  # niestety zdarzają są remisy
        if verbose:
            print("removing arc", arc_to_remove)
        outranking_matrix.at[arc_to_remove[0], arc_to_remove[1]] = 0

    assert not list(nx.simple_cycles(nx.from_numpy_array(outranking_matrix.to_numpy(), create_using=nx.DiGraph))), "został jeszcze cykl"

    np.fill_diagonal(outranking_matrix.values, 1)
    return outranking_matrix


def transitivity_correction(outranking_matrix_clusters, verbose):
    from itertools import permutations

    done = False
    while not done:
        done = True
        for a, b, c in list(permutations(range(len(outranking_matrix_clusters)), 3)):
            if outranking_matrix_clusters.at[a, b] and outranking_matrix_clusters.at[b, c] and not outranking_matrix_clusters.at[a, c]:
                outranking_matrix_clusters.at[a, c] = 1
                if verbose:
                    print("dodaję łuk", a, "-->", c)
                done = False
                break

    return outranking_matrix_clusters


# Preference structure is build based on the ranking of centroids
def calculate_clusters_relations_by_centroid(df, indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda, remove_cycles=True, correct_transitivity=True,
                                             verbose=False):
    credibility_matrix, outranking_matrix = calculate_credibility_and_outranking_matrix(df, indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda)

    if remove_cycles:
        outranking_matrix = cluster_outranking_remove_indifferences(outranking_matrix, credibility_matrix, verbose)
        outranking_matrix = cluster_outranking_remove_cycles(outranking_matrix, credibility_matrix, verbose)
    if correct_transitivity:
        outranking_matrix = transitivity_correction(outranking_matrix, verbose)

    return outranking_matrix


def outranking_degree(cluster1, cluster2, outranking_matrix):
    degree = 0
    for alternative1 in cluster1:
        for alternative2 in cluster2:
            degree += outranking_matrix.at[alternative1, alternative2]

    degree /= len(cluster1) * len(cluster2)
    return degree


# Preference structure is build based on outranking degree
def calculate_clusters_relations_by_outranking_degree(partition, outranking_matrix_alternatives, cluster_outranking_threshold=0.5, remove_cycles=True, correct_transitivity=True, verbose=False):
    n = len(partition)
    credibility_matrix = np.identity(n, dtype=float)
    outranking_matrix = np.identity(n, dtype=int)

    for i in range(n):
        for j in range(n):
            if i != j:
                degree = outranking_degree(partition[i], partition[j], outranking_matrix_alternatives)
                credibility_matrix[i, j] = degree
                outranking_matrix[i, j] = degree >= cluster_outranking_threshold

    outranking_matrix = pd.DataFrame(outranking_matrix, index=np.arange(n), columns=np.arange(n))
    credibility_matrix = pd.DataFrame(credibility_matrix, index=np.arange(n), columns=np.arange(n))

    if remove_cycles:
        outranking_matrix = cluster_outranking_remove_indifferences(outranking_matrix, credibility_matrix, verbose)
        outranking_matrix = cluster_outranking_remove_cycles(outranking_matrix, credibility_matrix, verbose)
    if correct_transitivity:
        outranking_matrix = transitivity_correction(outranking_matrix, verbose)

    return outranking_matrix


# Preference structure is build on the most common preference relation between alternatives in clusters
def calculate_clusters_relations_by_most_common_relation(partition, outranking_matrix_alternatives, remove_cycles=True, correct_transitivity=True, verbose=False):
    n = len(partition)
    credibility_matrix = np.identity(n, dtype=float)
    outranking_matrix = np.identity(n, dtype=int)

    for ci in range(n):
        for cj in range(ci + 1, n):
            ci_cj_relations = {"R": 0, "Pij": 0, "Pji": 0, "I": 0}

            pair_count = 0
            for ai in partition[ci]:
                for aj in partition[cj]:
                    pair_count += 1
                    alternatives_relation = get_relation_from_outranking_matrix(ai, aj, outranking_matrix_alternatives)
                    ci_cj_relations[alternatives_relation] += 1

            # normalize dict
            for key in ci_cj_relations:
                ci_cj_relations[key] /= pair_count

            most_common_relation = max(ci_cj_relations, key=ci_cj_relations.get)

            if most_common_relation == "Pij":
                outranking_matrix[ci, cj] = 1
                credibility_matrix[ci, cj] = ci_cj_relations["Pij"]
                outranking_matrix[cj, ci] = 0
                credibility_matrix[cj, ci] = 0
            elif most_common_relation == "Pji":
                outranking_matrix[ci, cj] = 0
                credibility_matrix[ci, cj] = 0
                outranking_matrix[cj, ci] = 1
                credibility_matrix[cj, ci] = ci_cj_relations["Pji"]
            elif most_common_relation == "R":
                outranking_matrix[ci, cj] = 0
                credibility_matrix[ci, cj] = 0
                outranking_matrix[cj, ci] = 0
                credibility_matrix[cj, ci] = 0
            else:  # indifference I
                outranking_matrix[ci, cj] = 1
                outranking_matrix[cj, ci] = 1
                credibility_matrix[ci, cj] = ci_cj_relations["Pij"]
                credibility_matrix[cj, ci] = ci_cj_relations["Pji"]

    outranking_matrix = pd.DataFrame(outranking_matrix, index=np.arange(n), columns=np.arange(n))
    credibility_matrix = pd.DataFrame(credibility_matrix, index=np.arange(n), columns=np.arange(n))

    if remove_cycles:
        outranking_matrix = cluster_outranking_remove_indifferences(outranking_matrix, credibility_matrix, verbose)
        outranking_matrix = cluster_outranking_remove_cycles(outranking_matrix, credibility_matrix, verbose)
    if correct_transitivity:
        outranking_matrix = transitivity_correction(outranking_matrix, verbose)

    return outranking_matrix
