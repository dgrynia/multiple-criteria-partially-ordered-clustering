from utils.common_tools import *


def prepare_electre_parameters(X, seed):
    rng = np.random.default_rng(seed=seed)

    a = [0] + sorted(rng.uniform(low=0, high=1, size=len(X.columns) - 1).round(4).tolist()) + [1]
    weights = [t - s for s, t in zip(a, a[1:])]

    cutting_level_lambda = rng.uniform(low=0.5, high=1 - min(weights), size=1).round(4)[0]

    params = np.zeros((len(X.columns), 4))
    for i, criterion in enumerate(X.columns):
        v_max = X[criterion].max() - X[criterion].min()

        v1, v2, v3 = rng.uniform(low=0, high=v_max, size=3).round(4)
        vi, vp, vr = sorted([v1, v2, v3])
        params[i] = [vi, vp, vr, weights[i]]

    params = pd.DataFrame(params, index=X.columns, columns=["q", "p", "v", "w"])

    indifference_thresholds = params["q"].tolist()
    preference_thresholds = params["p"].tolist()
    veto_thresholds = params["v"].tolist()
    weights = params["w"].tolist()

    return indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda


def electre_concordance(ai, aj, indifference_thresholds, preference_thresholds, weights):
    single_criterion_concordance_indices = []
    for t in range(len(ai)):
        if aj[t] - ai[t] >= preference_thresholds[t]:
            concordance = 0
        elif indifference_thresholds[t] < aj[t] - ai[t] < preference_thresholds[t]:
            concordance = (preference_thresholds[t] - aj[t] + ai[t]) / (preference_thresholds[t] - indifference_thresholds[t])
        else:  # aj[t] - ai[t] <= indifference_thresholds[t]
            concordance = 1

        single_criterion_concordance_indices.append(concordance)

    global_concordance_index = np.dot(single_criterion_concordance_indices, weights) / np.sum(weights)
    return single_criterion_concordance_indices, global_concordance_index


def electre_discordance(ai, aj, discordance_thresholds, veto_thresholds, weights):
    single_criterion_discordance_indices = []
    for t in range(len(ai)):
        if aj[t] - ai[t] >= veto_thresholds[t]:
            discordance = 1
        elif discordance_thresholds[t] < aj[t] - ai[t] < veto_thresholds[t]:
            discordance = (aj[t] - ai[t] - discordance_thresholds[t]) / (veto_thresholds[t] - discordance_thresholds[t])  # UWAGA w magisterce błąd we wzorze, w przykładach jest już OK
        else:  # aj[t] - ai[t] <= discordance_thresholds[t]
            discordance = 0

        single_criterion_discordance_indices.append(discordance)

    return single_criterion_discordance_indices


def electre_credibility(global_concordance_index, single_criterion_discordance_indices):
    credibility_degree_S = global_concordance_index
    for discordance in single_criterion_discordance_indices:
        if discordance > global_concordance_index:
            credibility_degree_S *= (1 - discordance) / (1 - global_concordance_index)

    return credibility_degree_S


def electre(ai, aj, indifference_thresholds, preference_thresholds, veto_thresholds, weights, discordance_thresholds=None):
    assert len(ai) == len(aj) == len(indifference_thresholds) == len(preference_thresholds) == len(veto_thresholds) == len(weights)
    if discordance_thresholds is None:
        discordance_thresholds = preference_thresholds

    single_criterion_concordance_indices, global_concordance_index = electre_concordance(ai, aj, indifference_thresholds, preference_thresholds, weights)
    single_criterion_discordance_indices = electre_discordance(ai, aj, discordance_thresholds, veto_thresholds, weights)

    credibility_degree_S = electre_credibility(global_concordance_index, single_criterion_discordance_indices)

    return credibility_degree_S


def calculate_credibility_and_outranking_matrix(df, indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda):
    assert 0 <= cutting_level_lambda <= 1

    n = len(df)
    credibility_matrix = np.identity(n, dtype=float)
    outranking_matrix = np.identity(n, dtype=int)

    for i in range(n):
        for j in range(n):
            if i != j:
                credibility = electre(df.iloc[i].to_numpy(), df.iloc[j].to_numpy(),
                                      indifference_thresholds, preference_thresholds, veto_thresholds, weights)
                credibility_matrix[i, j] = credibility
                outranking_matrix[i, j] = credibility >= cutting_level_lambda

    credibility_matrix = pd.DataFrame(credibility_matrix, index=df.index, columns=df.index)
    outranking_matrix = pd.DataFrame(outranking_matrix, index=df.index, columns=df.index)

    return credibility_matrix, outranking_matrix


def calculate_outranking_matrix_for_alternatives(df, indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda):
    credibility_matrix, outranking_matrix = calculate_credibility_and_outranking_matrix(df, indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda)

    return outranking_matrix
