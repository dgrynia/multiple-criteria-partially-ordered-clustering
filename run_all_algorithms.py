import os
import time
import pickle
import glob
from pathlib import Path
from tqdm import trange, tqdm

from utils.electre_tools import *
from utils.distance_measures import calculate_distance_matrix_MC_distance
from utils.experiment_results import ExperimentResultsCollector

from algorithms.kmeans import kmeans_repeated_run
from algorithms.kmedoids import kmedoids_clustering
from algorithms.agglomerative import agglomerative_clustering
from algorithms.divisive_distance_based import divisive_clustering
from algorithms.mean_shift import meanshift_clustering
from algorithms.som import som_clustering

from algorithms.kmeans_extension import kmeans_extension_repeated_run
from algorithms.agglomerative_MPOC import agglomarative_MPOC_PQ_clustering
from algorithms.agglomerative_MPOC import agglomarative_MPOC_RCI_clustering
from algorithms.divisive_intra_cluster_relations import divisive_intra_cluster_relations_clustering

from algorithms.adaptation_outranking_matrix import classical_algorithm_adaptation_outranking_matrix
from algorithms.kmodes import kmodes_clustering
from algorithms.nmf import nmf_clustering_sklearn
from algorithms.onmf import onpmf_clustering

real_world_datasets = sorted(glob.glob("datasets/real/*.csv"))
artificial_datasets = sorted(glob.glob("datasets/artificial/*.csv"))
num_clusters = [2, 3, 4, 5, 6, 7, 8]
NUMBER_OF_ELECTRE_CONFIGURATIONS = 30
SEED_ELECTRE_CONFIGURATIONS = 123456


def check_n_clusters(n_clusters, n_alternatives):
    if isinstance(n_clusters, int):
        if n_clusters > 1 and n_clusters < n_alternatives:
            n_clusters_list = [n_clusters]
        else:
            raise ValueError("number of clusters must be greater than 1 and less than number_of_alternatives")
    elif isinstance(n_clusters, list):
        if all([isinstance(i, int) and i > 1 and i < n_alternatives for i in n_clusters]):
            n_clusters_list = n_clusters
        else:
            raise ValueError("number of clusters must be greater than 1 and less than number_of_alternatives")
    else:
        raise TypeError("n_clusters must be int or list")

    return n_clusters_list


def run_with_multiple_electre_configurations(algorithm_description, clustering_function,
                                             alternatives, X, n_clusters,
                                             seed=SEED_ELECTRE_CONFIGURATIONS, n_electre_configurations=NUMBER_OF_ELECTRE_CONFIGURATIONS,
                                             distance_matrix_function=None, verbose=False, **clustering_function_kwargs):
    n_clusters_list = check_n_clusters(n_clusters, len(alternatives))
    seeds = generate_seeds(seed=seed, n=n_electre_configurations)

    for n_clusters in tqdm(n_clusters_list):
        experiment_results = ExperimentResultsCollector(algorithm_description, X, seed, n_electre_configurations, verbose)

        for i in trange(n_electre_configurations):
            indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda = prepare_electre_parameters(X, seed=seeds[i])
            experiment_results.collect_signle_run_parameters(seeds[i], indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda)
            outranking_matrix_alternatives = calculate_outranking_matrix_for_alternatives(X, indifference_thresholds, preference_thresholds, veto_thresholds, weights, cutting_level_lambda)

            t1 = time.process_time()
            if distance_matrix_function is not None:
                distance_matrix = distance_matrix_function(outranking_matrix_alternatives)
                result = clustering_function(alternatives, X, n_clusters, outranking_matrix_alternatives,
                                             distance_matrix=distance_matrix, seed=seeds[i], verbose=verbose, **clustering_function_kwargs)
            else:
                result = clustering_function(alternatives, X, n_clusters, outranking_matrix_alternatives, seed=seeds[i], verbose=verbose, **clustering_function_kwargs)
            t2 = time.process_time()
            partitioning_execution_time = t2 - t1

            if isinstance(result, tuple):
                partition = result[0]
                centroids = result[1]
            else:
                partition = result
                centroids = None

            experiment_results.collect_single_run_results(partition, centroids, outranking_matrix_alternatives, partitioning_execution_time)

        ################## END FOR i in electre_configurations LOOP ###################

        dirname = os.path.join("results", Path(dataset_name).stem)
        Path(dirname).mkdir(parents=True, exist_ok=True)  # make directory if it does not exist
        pickle_path = os.path.join(dirname, "{}#{}#{}_clusters.pickle".format(Path(dataset_name).stem, algorithm_description, n_clusters))
        print(f"Saving results to file: {pickle_path}\n")
        with open(pickle_path, 'wb') as file:
            pickle.dump(experiment_results, file)
        ################## END FOR n_clusters in n_clusters_list LOOP ###################


for dataset_name in tqdm(real_world_datasets + artificial_datasets):
    print(f"Running experiments for dataset: {Path(dataset_name).stem}")
    X = pd.read_csv(dataset_name, index_col=0)
    alternatives = X.index.tolist()

    # Classical clustering algorithms based on alternatives’ performances
    run_with_multiple_electre_configurations("kmeans_criteria", kmeans_repeated_run, alternatives, X, num_clusters)
    run_with_multiple_electre_configurations("kmedoids_criteria", kmedoids_clustering, alternatives, X, num_clusters, affinity="euclidean")
    run_with_multiple_electre_configurations("agglomerative_criteria", agglomerative_clustering, alternatives, X, num_clusters)
    run_with_multiple_electre_configurations("divisive_criteria", divisive_clustering, alternatives, X, num_clusters)
    run_with_multiple_electre_configurations("meanshift_criteria", meanshift_clustering, alternatives, X, num_clusters, dataset_name=Path(dataset_name).stem)
    run_with_multiple_electre_configurations("som_criteria", som_clustering, alternatives, X, num_clusters)

    # Dedicated algorithms for partially ordered clustering
    run_with_multiple_electre_configurations("kmeans_extension", kmeans_extension_repeated_run, alternatives, X, num_clusters)
    run_with_multiple_electre_configurations("agglomarative_MPOC_PQ", agglomarative_MPOC_PQ_clustering, alternatives, X, num_clusters)
    run_with_multiple_electre_configurations("agglomarative_MPOC_RCI", agglomarative_MPOC_RCI_clustering, alternatives, X, num_clusters)
    run_with_multiple_electre_configurations("divisive_intra_cluster_relations", divisive_intra_cluster_relations_clustering, alternatives, X, num_clusters)

    # Adaptations of classical clustering algorithms that employ information about the out- and ingoing outranking relations
    for input_matrix_type in ['outgoing_relations', 'incoming_relations', 'all_relations']:
        print("\n-------------------", input_matrix_type, "-------------------\n")

        run_with_multiple_electre_configurations(
            "kmodes_" + input_matrix_type, classical_algorithm_adaptation_outranking_matrix,
            alternatives, X, num_clusters,
            classical_clustering_algorithm=kmodes_clustering, input_matrix_type=input_matrix_type)

        run_with_multiple_electre_configurations(
            "kmedoids_" + input_matrix_type, classical_algorithm_adaptation_outranking_matrix,
            alternatives, X, num_clusters,
            classical_clustering_algorithm=kmedoids_clustering, input_matrix_type=input_matrix_type)

        run_with_multiple_electre_configurations(
            "agglomerative_" + input_matrix_type, classical_algorithm_adaptation_outranking_matrix,
            alternatives, X, num_clusters,
            classical_clustering_algorithm=agglomerative_clustering, input_matrix_type=input_matrix_type)

        run_with_multiple_electre_configurations(
            "divisive_" + input_matrix_type, classical_algorithm_adaptation_outranking_matrix,
            alternatives, X, num_clusters,
            classical_clustering_algorithm=divisive_clustering, input_matrix_type=input_matrix_type)

    # Adaptations of classical pairwise clustering algorithms combined with multiple criteria distance measure
    run_with_multiple_electre_configurations(
        "kmedoids_MC_distance", kmedoids_clustering,
        alternatives, X, num_clusters,
        distance_matrix_function=calculate_distance_matrix_MC_distance,
        affinity="precomputed")

    run_with_multiple_electre_configurations(
        "agglomerative_MC_distance", agglomerative_clustering,
        alternatives, X, num_clusters,
        distance_matrix_function=calculate_distance_matrix_MC_distance,
        affinity="precomputed")

    run_with_multiple_electre_configurations(
        "divisive_MC_distance", divisive_clustering,
        alternatives, X, num_clusters,
        distance_matrix_function=calculate_distance_matrix_MC_distance,
        affinity="precomputed")

    # NMF-based clustering algorithms
    for input_matrix_type in ['outgoing_relations', 'incoming_relations', 'all_relations']:
        print("\n-------------------", input_matrix_type, "-------------------\n")

        # Classical NMF from scikit-learn library
        run_with_multiple_electre_configurations(
            "NMF_" + input_matrix_type, nmf_clustering_sklearn,
            alternatives, X, num_clusters,
            input_matrix_type=input_matrix_type, dataset_name=Path(dataset_name).stem)

        # Orthogonal NMF from https://github.com/filippo-p/onmf
        # To perform ONMF clustering, please first run the MATLAB script located at: algorithms/ONPMF/run_onpmf_clustering.m
        run_with_multiple_electre_configurations(
            "ONMF_" + input_matrix_type, onpmf_clustering,
            alternatives, X, num_clusters,
            input_matrix_type=input_matrix_type, dataset_name=Path(dataset_name).stem)
